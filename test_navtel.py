# coding=utf-8
import unittest
from unittest import TestCase

import requests
import dumper
import config


class TestNavtel(TestCase):

    def test_state(self):
        s = requests.Session()
        r = s.get(config.base_url + '/devices')
        self.assertEqual(r.status_code, 200, 'Неверный код ответа')
        clients = r.json()
        self.assertTrue(clients, 'Нет подключенных клиентов')
        client_id = clients[0]['id']
        self.assertIsNotNone(client_id, 'Пустой id')

        r = s.get(config.base_url + '/devices/000')
        self.assertEqual(r.status_code, 404, 'Неверный код ответа')

        r = s.get(config.base_url + '/devices/' + str(client_id))
        self.assertEqual(r.status_code, 200, 'Неверный код ответа')
        self.assertEqual(r.json()['id'], client_id, 'Неверный id')

        r = s.get(config.base_url + '/devices/' + str(client_id) + '/state')
        self.assertEqual(r.status_code, 200, 'Неверный код ответа')

    def test_model(self):
        s = requests.Session()
        r = s.get(config.base_url + '/devices')
        self.assertEqual(r.status_code, 200, 'Неверный код ответа')
        clients = r.json()
        self.assertTrue(clients, 'Нет подключенных клиентов')
        client_id = clients[0]['id']
        self.assertIsNotNone(client_id, 'Пустой id')

        r = s.get(config.base_url + '/devices/' + str(client_id))
        self.assertEqual(r.status_code, 200, 'Неверный код ответа')
        self.assertEqual(r.json()['id'], client_id, 'Неверный id')

        r = s.get(config.base_url + '/devices/' + str(client_id) + '/model')
        self.assertEqual(r.status_code, 200, 'Неверный код ответа')

if __name__ == '__main__':
    unittest.main()