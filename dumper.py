import requests
from flask import json


def dump_headers(headers):
    for header in headers:
        print header + ': ' + headers[header]


def dump_request(r):
    print '----- Request -----'
    print
    print r.url
    dump_headers(r.headers)
    print


def dump_response(r, *args, **kw):
    print '----- Response -----'
    print
    print 'HTTP ' + str(r.status_code)
    dump_headers(r.headers)
    if r.text:
        print
        print
        if 'content-type' in r.headers and 'json' in r.headers['content-type']:
            print json.dumps(r.json(), indent=4, sort_keys=True)
        else:
            print r.text
    print
    print
    print


def _send(*args, **kw):
    dump_request(args[1])
    return _origin_send(*args, **kw)


def _request(*args, **kw):
    global _origin_request
    if not 'hooks' in kw:
        kw['hooks'] = {'response': dump_response}
    return _origin_request(*args, **kw)


_origin_send = getattr(requests.Session, 'send')
setattr(requests.Session, 'send', _send)

_origin_request = getattr(requests.Session, 'request')
setattr(requests.Session, 'request', _request)
